// destructuring
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// export default function Banner({ title, subtitle, buttonText, buttonLink }) {
//   return (
//     <Row>
//       <Col className="p-5 text-center">
//         <h1>{title}</h1>
//         <p>{subtitle}</p>
//         <Button variant="primary" href={buttonLink}>
//           {buttonText}
//         </Button>
//       </Col>
//     </Row>
//   );
// }

export default function Banner ({data}) {
	const {title, content, destination, label} = data;
	console.log(data);

	return(

		<Row>
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>

	)
}