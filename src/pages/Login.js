import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Login() {

        // Allows us to consume the UserContext object and it's properties for user validation
        const { user, setUser } = useContext(UserContext);

        // State hooks to store the values of our input fields
        const [email, setEmail] = useState('');
        const [password, setPassword] = useState('');
        // State to determine whether the submit button is enabled or not
        const [isActive, setIsActive] = useState(false);

        console.log(email);
        console.log(password);

        // useEffect() - whenever there is a change in our webpage
        useEffect(() => {
          if (email !== '' && password !== '') {
            setIsActive(true);
          } else {
            setIsActive(false);
          }
        }, [email, password]);


        function loginUser(e) {

            e.preventDefault();

            // Process a fetch request to the corresponding backend API
            fetch('http://localhost:4000/users/login', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                }),
            })
            .then(res => res.json())
            .then(data => {

                console.log(data)

                // If no user information
                if (typeof data.access !== "undefined") {

                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);

                    Swal.fire({
                        title: "Authentication Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })

                } else {

                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "Check your login details and try"
                    })
                }

            })

            // Set the email of the authenticated user in the local storage
            // localStorage.setItem("email", email)

            // user = {email: localStorage.getItem('email')}
            // setUser({
            //     email: localStorage.getItem('email')
            // })

            // clear input fields
            setEmail('');
            setPassword('');

            // alert('Successful login');
        }

        const retrieveUserDetails = (token) => {

            // The token will be sent as part of the requests's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch('http://localhost:4000/users/details', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // Changes the globale "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser ({
                    id: data._id,
                    isAdmin: data.isAdmin,
                })
            })
        }

        return (
            (user.id !== null) ?
                <Navigate to="/courses" />
            :
            <Form onSubmit={e => loginUser(e)}>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
                <Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </Form.Text>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
              </Form.Group>

              {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
              }
              
            </Form>
        )
}
