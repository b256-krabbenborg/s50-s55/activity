// Register.js
import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import AuthCheck from './AuthCheck';
import Swal from 'sweetalert2';

function RegisterComponent() {
  // State hooks to store the values of our input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  // State to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  console.log(firstName);
  console.log(lastName);
  console.log(mobileNumber);
  console.log(email);
  console.log(password1);
  console.log(password2);

  // useEffect() - whenever there is a change in our webpage

  useEffect(() => {
  if (
    firstName !== '' &&
    lastName !== '' &&
    mobileNumber !== '' &&
    mobileNumber.length >= 11 &&
    email !== '' &&
    password1 !== '' &&
    password2 !== '' &&
    password1 === password2
  ) {
    setIsActive(true);
  } else {
    setIsActive(false);
  }
}, [firstName, lastName, mobileNumber, email, password1, password2]);


  async function registerUser(e) {
    e.preventDefault();

    // Check if email already exists before registering
    const response = await fetch('http://localhost:4000/users/checkemail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({email }),
    });

    if (response.status === 200) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'This email is already registered. Please try again.',
        });
      } else {
        // Register the user
        await fetch('https://your-api-url/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ firstName, lastName, mobileNumber, email, password: password1 }),
        });

        // clear input fields
        setFirstName('');
        setLastName('');
        setMobileNumber('');
        setEmail('');
        setPassword1('');
        setPassword2('');

        Swal.fire({
          icon: 'success',
          title: 'Thank you for registering!',
          showConfirmButton: false,
          timer: 1500,
        }).then(() => {
          navigation('/login');
        });
      }
    }


  // onChange - checks if there are any changes inside of the input fields.
  // value={email} - the value stroed in the input field will come from the value inside the getter "email"
  // setEmail(e.target.value) - sets the value of the getter ëmail to the value stored in the value={email} inside the input field
  return (
    <Form onSubmit={(e) => registerUser(e)}>
      {/* Add form inputs for firstName, lastName, and mobileNumber */}
      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicMobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter mobile number"
          value={mobileNumber}
          onChange={(e) => setMobileNumber(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </Form.Group>

      {/*
        Ternary Operator
        if (isActive === true) {
            <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
        } else {
            <Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
        }
      */}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}

export default RegisterComponent;
