import React, { useContext } from 'react';
import { Outlet, Navigate } from 'react-router-dom';
import UserContext from '../userContext';

export default function AuthCheck() {
  const { user } = useContext(UserContext);

  return user.email ? (
    <Outlet />
  ) : (
    <Navigate to="/login" replace />
  );
}